// The following functions were addapted from the book 2D Game Collision Detection
//Authour Thomas Schwarzl
//Adapted by Josh Carberry 2022




class Vector2D{
    x
    y

    constructor(x=0,y=0){
        this.x = x
        this.y = y
    }

    set(x,y){
        this.x = x
        this.y = y
    }
}

function add_vector(a = new Vector2D(),b= new Vector2D()){
    return new Vector2D(a.x+b.x,a.y+b.y)
}

function subtract_vector(a,b){
    return new Vector2D(a.x-b.x,a.y-b.y)
}

function negate_vector(v){
    return new Vector2D(-v.x,-v.y)
}

function equal_floats(a,b){
//    var threshold = Number.EPSILON
    var threshold = 1/8192
    return Math.abs(a-b) < threshold
}

//function equal_vectors(a,b){
//    return equal_floats(a.x,b.x) && equal_floats(a.y,b.y)
//}

function multiply_vector(v,scalar){
    return new Vector2D(v.x * scalar,v.y * scalar)
}

function divide_vector(v,divisor){
    return new Vector2D(v.x/divisor,v.y/divisor)
}

function vector_length(v){
    return Math.sqrt(v.x * v.x + v.y *v.y)
}

function unit_vector(v){
    var length = vector_length(v)
    if(0< length)
        return divide_vector(v,length)
    return v
}

function degrees_to_radian(degrees){
    //var pi = 3.14159265358979323846 // we dont actually want infinite precsion
    return degrees * Math.PI/180
}

function rotate_vector(v,degrees){
    var radian = degrees_to_radian(degrees)
    var sine = Math.sin(radian)
    var cosine = Math.cos(radian)
    var r = new Vector2D()
    r.x = v.x * cosine - v.y * sine
    r.y = v.x * sine + v.y * cosine

    if(equal_floats(r.x,0))
        r.x = 0
    
    if(equal_floats(r.y,0))
        r.y = 0
    return r
}

function dot_product(a,b){
    return a.x * b.x + a.y * b.y
}

function radian_to_degrees(radian){
    return radian * 180 / Math.PI
}

function enclosed_angle(a,b){
    var ua = unit_vector(a)
    var ub = unit_vector(b)
    var dp = dot_product(ua,ub)
    return radian_to_degrees(Math.acos(dp))
}

function project_vector(project,onto){
    var d = dot_product(onto,onto)
    if(0< d){
        var dp = dot_product(project,onto)
        return multiply_vector(onto,dp/d)
    }
    return onto
}

function clone_vector(v){
    return new Vector2D(v.x,v.y)
}

function draw_vector(v){
    line(0,0,v.x,v.y)
}

function drawTestVectors(){
    push()
    translate(250, 250)
    scale(10,-10)

    var a = new Vector2D()
    var b = new Vector2D()
    var c = new Vector2D()

    a.set(12,5)
    b.set(5,6)

    c = project_vector(b,a)
    
    stroke('#00ff')
    draw_vector(a)
    stroke('#0f0f')
    draw_vector(b)
    stroke('#f00f')
    draw_vector(c)
    pop()
}

function testVectors(){
    console.log("test vectors")
    var a = new Vector2D()
    var b = new Vector2D()
    var c = new Vector2D()

    a.set(3,5)
    b.set(8,2)

    c = add_vector(a,b)
    console.log(c) //Vector2D {x: 11, y: 7}

    a.set(7,4)
    b.set(3,-3)

    c = subtract_vector(a,b)
    console.log(c) //Vector2D {x: 4, y: 7}
 
    a.set(7,4)
    b.set(3,-3)
    c = add_vector(a,negate_vector(b))


    console.log(c)//Vector2D {x: 4, y: 7}
    console.log(equal_vectors(c,subtract_vector(a,b)))//true

    a.set(6,3)
    b = multiply_vector(a,2)
    console.log(b)//12 6

    a.set(8,4)
    b = divide_vector(a,2)
    console.log(b)//4 2

    
    a.set(10,5)
    console.log(1 < vector_length(a))
    b = unit_vector(a)
    console.log(equal_floats(1,vector_length(b)))

    a.set(12,0)
    b = rotate_vector(a,45)
    console.log(b)   

    a.set(8,2)
    b.set(-2,8)
    c.set(-5,5)

    console.log(equal_floats(0,dot_product(a,b)))
    console.log(0 > dot_product(a,c))
    console.log(0 < dot_product(b,c))
    console.log(dot_product(b,c))

    a.set(8,0)
    b.set(4,4)
    console.log(enclosed_angle(a,b))

    a.set(12,5)
    b.set(5,6)

    c = project_vector(a,b)
    console.log(c)
}