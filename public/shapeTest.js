
function test_oriented_rectangles_collide(){
    var inActive = OrientedRectangle.create(200,200,200,20,frameCount/2)
    var Active = OrientedRectangle.create(mouseX,mouseY,10,10,90)

    var result = oriented_rectangles_collide(inActive,Active)
   // fill(result? '#0f0f':'#f00f')
    stroke(result? '#0f0f':'#f00f')
    inActive.draw()
    Active.draw()
}

function test_oriented_rectangles_collide2(){
    var distanceX = sin(frameCount/200) 
    var distanceY = cos(frameCount/200) 

    var inActive = OrientedRectangle.create(250,250,200,200,frameCount)
    var Active = Circle.create(250 - distanceX*150,250 + distanceX*150,45)

    stroke('#000f')
    fill('#dddf')

    var result = oriented_rectangle_point_collide(inActive,{x:mouseX,y:mouseY})
    fill(result? '#0f0f':'#dddf')
    
    inActive.draw()
    
    var result = circle_oriented_rectangle_collide(Active,inActive)
    fill(result? '#0f0f':'#f00f')
    Active.draw()

    Active = Segment.create(250 - distanceX*80,250 - distanceY*200,100,100)
    var result = oriented_rectangle_segment_collide(inActive,Active)
    stroke(result? '#0f0f':'#f00f')
    Active.draw()


    Active = Line.create(250,400,distanceY,-distanceX)
    var result = line_oriented_rectangle_collide(Active,inActive)
    stroke(result? '#0f0f':'#f00f')
    Active.draw()

    Active = Rectangle.create(250 ,250- (distanceY)*200,40,40)
    Active = Rectangle.create(mouseX ,mouseY,40,40)

    
    stroke('#000f')
    result = oriented_rectangle_rectangle_collide(inActive,Active)
    fill(result? '#0f0f':'#f00f')
    Active.draw()

    noFill()
    var hull = oriented_rectangle_rectangle_hull(inActive,Active)
    hull = enlarge_rectangle_rectangle(Active,hull)
    //hull.draw()

    var rectArr = []
    for(var i = 0; i< 10; i++){
        rectArr.push(Rectangle.create(mouseX + 15*i,mouseY + 15 *i,10,10))
        rectArr[i].draw()
    }

    var hull2 = rectangles_rectangle_hull(rectArr)
   // hull2.draw()

    hull = enlarge_rectangle_rectangle(hull,hull2)

    hull.draw()

    Active = OrientedRectangle.create(350,350,150,10,distanceX*90+45)
    result = oriented_rectangles_collide(Active,inActive)
    fill(result? '#0f0f':'#f00f')

    Active.draw()

    stroke('#000f')
    noFill()
    //fill('#dddf')
   var hulledcircle = oriented_rectangle_circle_hull(inActive)
    hulledcircle.draw()



}

function test_rectangles_collide(){
    var inActive = Rectangle.create(250,250,95,45)
    var Active = Rectangle.create(mouseX,mouseY,45,45)

    var result = rectangles_collide(inActive,Active)
    fill(result? '#0f0f':'#f00f')
    inActive.draw()
    Active.draw()
}

function test_circles_collide(){
    var distanceX = sin(frameCount/200) 
    var distanceY = cos(frameCount/200) 

    var inActive = Circle.create(250,250,95,45)
    var Active = Circle.create(250 - distanceX*150,250 + distanceX*150,45)

    stroke('#000f')
    fill('#dddf')
    
    inActive.draw()
    
    var result = circles_collide(inActive,Active)
    fill(result? '#0f0f':'#f00f')
    Active.draw()

    noFill()

    stroke('#f0ff')
    var circleArr = [inActive,Active]
    var hull = circles_circle_hull(circleArr)
    hull.draw()
    


    Active = Segment.create(250 - distanceX*80,250 - distanceY*200,100,100)
    var result = circle_segment_collide(inActive,Active)
    stroke(result? '#0f0f':'#f00f')
    Active.draw()


    Active = Line.create(250,400,distanceY,-distanceX)
    var result = circle_line_collide(inActive,Active)
    stroke(result? '#0f0f':'#f00f')
    Active.draw()

    Active = Rectangle.create(250 ,250- (distanceY)*200,10,10)

    
    stroke('#000f')
    result = circle_rectangle_collide(inActive,Active)
    fill(result? '#0f0f':'#f00f')
    Active.draw()

    Active = OrientedRectangle.create(350,350,150,10,distanceX*90+45)
    result = circle_oriented_rectangle_collide(inActive,Active)
    fill(result? '#0f0f':'#f00f')

    Active.draw()
}

function test_rectangles_collide2(){
    var distanceX = sin(frameCount/200) 
    var distanceY = cos(frameCount/200) 

    var inActive = Rectangle.create(250-50,250-50,90*2,50*2)
    var Active = Circle.create(250 - distanceX*150,250 + distanceX*150,45)

    stroke('#000f')
    fill('#dddf')

    var result = point_rectangle_collide({x:mouseX,y:mouseY},inActive)
    fill(result? '#0f0f':'#dddf')
    
    inActive.draw()
    
    var result = circle_rectangle_collide(Active,inActive)
    fill(result? '#0f0f':'#f00f')
    Active.draw()

    Active = Segment.create(250 - distanceX*80,250 - distanceY*200,100,100)
    var result = rectangle_segment_collide(inActive,Active)
    stroke(result? '#0f0f':'#f00f')
    Active.draw()


    Active = Line.create(250,400,distanceY,-distanceX)
    var result = line_rectangle_collide(Active,inActive)
    stroke(result? '#0f0f':'#f00f')
    Active.draw()

    Active = Rectangle.create(250 ,250- (distanceY)*200,10,10)

    
    stroke('#000f')
    result = rectangles_collide(inActive,Active)
    fill(result? '#0f0f':'#f00f')
    Active.draw()

    Active = OrientedRectangle.create(350,350,150,10,distanceX*90+45)
    result = oriented_rectangle_rectangle_collide(Active,inActive)
    fill(result? '#0f0f':'#f00f')

    Active.draw()
}

function test_lines_collide(){
    var a = new Vector2D(200,200)
    var b = new Vector2D(300,2)
    var c = new Vector2D(100,200)

    var down = new Vector2D(0,-1)
    var up = new Vector2D(0,1)

    var l1 = new Line(a,down)
    var l2 = new Line(c,new Vector2D(mouseX,mouseY))

    var inActive = l1
    var Active = l2

    var result = lines_collide(inActive,Active)
    stroke(result? '#0f0f':'#f00f')
    inActive.draw()
    Active.draw()
}

function test_segments_collide(){
    var inActive = Segment.create(200,200-(PI*cos(frameCount/100))*10,250+(PI*sin(frameCount/100))*10,200)
    var Active = Segment.create(mouseX,mouseY,mouseX+5,mouseY-9)

    var result = segments_collide(inActive,Active)
   
    stroke(result? '#0f0f':'#f00f')
    inActive.draw()
    Active.draw()
}

function test_point_collide(){
    var inActive = Circle.create(100,100,50)
    //var Active = new Vector2D(mouseX,mouseY)

    stroke('#000f')
    var mouseV = new Vector2D(mouseX,mouseY)
    var result = circle_point_collide(inActive,mouseV)
    fill(result? '#0f0f':'#f00f')
    inActive.draw()

    inActive = Rectangle.create(300-50,100-50,100,100)
    result = point_rectangle_collide(mouseV,inActive)
    fill(result? '#0f0f':'#f00f')
    inActive.draw()

    inActive = Line.create(400,200,1,0)
    result = line_point_collide(inActive,mouseV)
    

    var inActive2 = Segment.create(200,50,200,350)
    result = point_segment_collide(mouseV,inActive2)
    var result2 = line_segment_collide(inActive,inActive2)
    stroke(result || result2? '#0f0f':'#f00f')
    inActive2.draw()
    inActive.draw()

    inActive = OrientedRectangle.create(100,300,100,100,frameCount)
    
    stroke('#000f')
    result = oriented_rectangle_point_collide(inActive,mouseV)
    fill(result? '#0f0f':'#f00f')
    inActive.draw()
    
}

function test_circle_rectangle_tunneling(){
    var A = Circle.create(100,200,30,30)
    //var B = Circle.create(400,200,30,30)

    var move = subtract_vector({x:mouseX,y:mouseY},A.center)

    var wall = Rectangle.create(300,100,10,200)

    noFill()
    stroke('#111f')
    A.draw()

    wall.draw()
    
    result = moving_circle_rectangle_collide(A,move,wall)

    stroke('#111f')
    fill(result? '#0f0f':'#f00f')

    A.center = add_vector(A.center,move)
    A.draw()
}

function test_rectangle_tunneling(){
    var A = Rectangle.create(100,200,30,30)
    //var B = Circle.create(400,200,30,30)

    var move = subtract_vector({x:mouseX,y:mouseY},A.origin)

    var wall = Rectangle.create(300,100,10,200)

    noFill()
    stroke('#111f')
    A.draw()

    wall.draw()
    
    stroke('#1118')
    result = moving_rectangle_rectangle_collide(A,move,wall)

    stroke('#111f')
    fill(result? '#0f0f':'#f00f')

    A.origin = add_vector(A.origin,move)
    A.draw()
}

function test_circle_rectangle_tunneling(){
    var A = Circle.create(100,200,30,30)
    //var B = Circle.create(400,200,30,30)

    var move = subtract_vector({x:mouseX,y:mouseY},A.center)

    var wall = Circle.create(300,200,60)

    noFill()
    stroke('#111f')
    A.draw()

    wall.draw()
    
    result = moving_circle_circle_collide(A,move,wall)

    stroke('#111f')
    fill(result? '#0f0f':'#f00f')

    A.center = add_vector(A.center,move)
    A.draw()
}

function test_collision(){
    var testCase = 6
    switch(testCase){
        case 0:
            test_rectangles_collide2()
            break;
        case 1:
            test_circles_collide()
            break;
        case 2:
            test_lines_collide()
            break;
        case 3:
            test_segments_collide()
            break;
        case 4:
            test_oriented_rectangles_collide2()
            break;
        case 5:
            test_point_collide()
            break;
        case 6:
            test_circle_rectangle_tunneling()
            break;
        case 7:
            test_rectangle_tunneling()
            break;
        case 8:
            test_circle_tunneling()
    }
}