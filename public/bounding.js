

/*function oriented_rectangle_rectangle_hull(r = new OrientedRectangle()){
    var h = new Rectangle()
    h.origin = r.center
    
    var corner = new Vector2D()

    for(var nr = 0; nr < 4;nr++){
        corner = oriented_rectangle_corner(r,nr)
        h = enlarge_rectangle_point(h,corner)
    }

    return h
}*/


function oriented_rectangle_circle_hull(r = new OrientedRectangle()){
    var h = new Circle()
    h.center = r.center
    h.radius = vector_length(r.halfExtend)

    return h
}

function enlarge_rectangle_rectangle(r = new Rectangle(), extender = new Rectangle()){
    var maxCorner = add_vector(extender.origin,extender.size)
    var enlarged = enlarge_rectangle_point(r,maxCorner)

    return enlarge_rectangle_point(enlarged,extender.origin)
}

function rectangles_rectangle_hull(rectangles = []){
    var h = new Rectangle()
    if(0 == rectangles.length)
        return h
    
    h = rectangles[0]
    for(var i =0; i < rectangles.length;i++){
        h = enlarge_rectangle_rectangle(h,rectangles[i])
    }

    return h
}

function circles_rectangle_hull(circles =[]){
    h = new Rectangle()
    if(circles.length == 0)
        return h
    
    h.origin = circles[0].center

    var halfExtend = new Vector2D()
    for(var i =0; i < circles.length;i++){
        halfExtend.x = circles[i].radius
        halfExtend.y = circles[i].radius
        var minP = subtract_vector(circles[i].center,halfExtend)
        var maxP = add_vector(circles[i].center,halfExtend)
        h = enlarge_rectangle_point(h,minP)
        h = enlarge_rectangle_point(h,maxP)
    }

    return h
}

function rectangle_center(r = new Rectangle()){
    halfSize = divide_vector(r.size,2)
    return add_vector(r.origin,halfSize)
}

function lerp_vectors(a,b,amt){
    var r = new Vector2D()
    r.x = lerp(a.x, b.x, amt)
    r.y = lerp(a.y,b.y,amt)
    return r
}

function circles_circle_hull(circles =[]){
    var h = new Circle()
    if(circles.length == 0)
        return h
    
    var rh = circles_rectangle_hull(circles)
    rh.draw()
    h.center = rectangle_center(rh)

    ellipse(h.center.x, h.center.y, 5, 5)

    
    for(var i =0; i < circles.length;i++){
   //     var ph = h
        var d = subtract_vector(circles[i].center,h.center)
        var extension = vector_length(d) + circles[i].radius
        
        h.radius = abs_maximum(extension,h.radius)
      

        //line(ph.x, ph.y, h, y2)
        ellipse(h.center.x, h.center.y, 5, 5)

    }

    return h
}

function moving_circle_rectangle_collide(a = new Circle(),moveA = new Vector2D(),b = new Rectangle()){
    var envelope = new Circle()
    var halfMoveA = divide_vector(moveA,2)
    var moveDistance = vector_length(moveA)

    envelope.center = add_vector(a.center,halfMoveA)
    envelope.radius = a.radius + moveDistance /2

    if(circle_rectangle_collide(envelope,b)){
       // stroke('#f0ff')
       // envelope.draw()
        var episilonf = 1.0 / 32
        var minimumMoveDistance = abs_maximum(a.radius/8,episilonf)

        if(moveDistance < minimumMoveDistance)
        {
            stroke('#f0ff')
            a.draw()
            noFill()
            return true
        }
        envelope.radius = a.radius
        return moving_circle_rectangle_collide(a,halfMoveA,b)
                ||
            moving_circle_rectangle_collide(envelope,halfMoveA,b);
    }else
    return false

}

function moving_rectangle_rectangle_collide(a = new Rectangle(),moveA= new Vector2D(),b = new Rectangle()){
    var envelope = new Rectangle(a.origin,a.size)
    envelope.origin = add_vector(envelope.origin,moveA)
    envelope = enlarge_rectangle_rectangle(envelope,a)

    if(rectangles_collide(envelope,b)){
        envelope.draw()
        var epsilonf = 1.0 /32
        var min = abs_minimum(a.size.x,a.size.y)/4
        var minimumMoveDistance = abs_maximum(min,epsilonf)

        var halfMoveA = divide_vector(moveA,2)

        if(vector_length(moveA) < minimumMoveDistance){
            stroke('#f0ff')
            a.draw()
            noFill()
            return true
        }

        envelope.origin = add_vector(a.origin,halfMoveA)
        envelope.size = a.size
        return moving_rectangle_rectangle_collide(a,halfMoveA,b)
        ||
        moving_rectangle_rectangle_collide(envelope,halfMoveA,b)
    }
    return false
}

function moving_circle_circle_collide(a= new Circle(),moveA= new Vector2D(),b=new Circle()){
    var bAbsorbedA = new Circle()
    bAbsorbedA.center = b.center
    bAbsorbedA.radius = a.radius + b.radius

    var travelA = new Segment();
    travelA.point1 = a.center
    travelA.point2 = add_vector(a.center,moveA)   

    stroke('#f0ff')
    bAbsorbedA.draw()
    travelA.draw()

    return circle_segment_collide(bAbsorbedA,travelA)
}

function moving_rectangle_circle_collide(a = new Rectangle(),moveA = new Vector2D(),b= new Circle()){
    var moveB = negate_vector(moveA)
    return moving_circle_rectangle_collide(b,moveB,a)
}